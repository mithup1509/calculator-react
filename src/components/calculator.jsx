import React, { Component } from "react";
import Displaybar from "./displaybar";
import Numbers from "./numbers";

class Calculator extends Component {
  state = {
    value: "",
  };

  buttonValue = (e) => {
  
    if (e.target.value !== "="  ) {
      
      this.setState({ value: this.state.value + e.target.value });
    }
  };

  performOperation = () => {
    
    try {
      
      let evaluation = eval(this.state.value);
      this.setState({ value: `${evaluation}` });
    
    } catch (err) {
      let evaluation = "Error";
      this.setState({ value: evaluation });
    }
  };
  cleardisplay = () => {
    this.setState({ value: "" });
  };

  removelast = () => {
    let val = "" + this.state.value;

    this.setState({ value: val.slice(0, val.length - 1) });
  };
  render() {
    return (
      <div style={{display:'flex',flexDirection:'column',justifyContent:'center',alignItems:"center" ,width:1440}}>
        <Displaybar data={this.state.value} />
        <Numbers
          onButtonvalue={this.buttonValue}
          onperformOperation={this.performOperation}
          onCleardisplay={this.cleardisplay}
          onRemovelast={this.removelast}
        />
      </div>
    );
  }
}

export default Calculator;
