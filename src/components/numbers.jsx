import React, { Component } from 'react';
import Buttons from './buttons';

class Numbers extends Component {
    styles={
        border:"2px solid black",
        width:500,
    }

 
    render() { 
        return (
            <div style={this.styles}>
                <Buttons onButtonvalue={this.props.onButtonvalue}
                 onperformOperation={this.props.onperformOperation}
                  onCleardisplay={this.props.onCleardisplay}
                  onRemovelast={this.props. onRemovelast}/>
            </div>
        );
    }
}
 
export default Numbers;