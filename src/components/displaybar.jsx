import React, { Component } from 'react';


class Displaybar extends Component {
 
    styles={
        border:"2px solid black",
        width:500,
        height:125,
        backgroundColor:"#964B00",
        overflowX:"hidden",
        display:"flex",
        justifyContent:'flex-end'

    }
    render() { 
        return (
            <div style={this.styles}>
            <h1>{this.props.data.slice(0,10)}</h1>
            </div>
        );
    }
}
 
export default Displaybar;