import React, { Component } from 'react';

class Buttons extends Component {
    styles = {
        border:'1px solid black',
        width:125,
        height:75,
        
      } 

    render() { 
    
        return (
            <React.Fragment>
               
                    <button style={{ border:'1px solid black',width:250,height:75,backgroundColor:"green"}}  value={'clear'} onClick={this.props.onCleardisplay}>Clear</button>  
                <button style={this.styles} value={1} onClick={this.props.onButtonvalue}>1</button>
                <button style={this.styles} value={2} onClick={this.props. onButtonvalue}>2</button>
                <button style={{ border:'1px solid black',width:125,height:75,backgroundColor:"red"}} value={'-'} onClick={this.props. onButtonvalue}>-</button>
                <button style={this.styles} value={3} onClick={this.props. onButtonvalue}>3</button>
                <button style={this.styles} value={4} onClick={this.props. onButtonvalue}>4</button>
                <button style={this.styles} value={5} onClick={this.props. onButtonvalue}>5</button>
                <button style={{ border:'1px solid black',width:125,height:75,backgroundColor:"red"}}  value={'+'} onClick={this.props. onButtonvalue}>+</button>
                <button style={this.styles} value={6} onClick={this.props. onButtonvalue}>6</button>
                <button style={this.styles} value={7} onClick={this.props. onButtonvalue}>7</button>
                <button style={this.styles} value={8} onClick={this.props. onButtonvalue}>8</button>
                <button style={{ border:'1px solid black',width:125,height:75,backgroundColor:"red"}}  value={'*'} onClick={this.props. onButtonvalue}>X</button>
                <button style={this.styles} value={9} onClick={this.props. onButtonvalue}>9</button>
                <button style={this.styles} value={0} onClick={this.props. onButtonvalue}>0</button>
                <button style={{ border:'1px solid black', width:125,height:75,backgroundColor:"#808080"}}  onClick={this.props. onRemovelast}>C</button>
               <button style={{ border:'1px solid black',width:125,height:75,backgroundColor:"red"}}  value={'/'} onClick={this.props. onButtonvalue}>%</button>
                <button style={{ border:'1px solid black',width:125,height:75,backgroundColor:"red"}}  value={'('} onClick={this.props. onButtonvalue}>(</button>
                <button style={{ border:'1px solid black',width:125,height:75,backgroundColor:"red"}}  value={')'} onClick={this.props. onButtonvalue}>)</button>
                <button style={{ border:'1px solid black',width:125,height:75,backgroundColor:"brown"}}  value={'='} onClick={this.props.onperformOperation}>=</button>
               
            
            </React.Fragment>
        );
    }
}
 
export default Buttons;