import React from 'react'
import ReactDOM from 'react-dom/client'
// import App from './App.jsx'
import './index.css'
import Calculator from './components/calculator'

ReactDOM.createRoot(document.getElementById('root')).render(
  
    <Calculator />
)
